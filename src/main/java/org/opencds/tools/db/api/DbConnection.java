/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.db.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Iterator;
import java.util.function.Consumer;
import java.util.function.Supplier;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DbConnection {
    private static final Log log = LogFactory.getLog(DbConnection.class);

    public enum DbProperty {
        AUTOCOMMIT{
            @Override
            public <T> void setValue(Connection conn, T value) {
                if (value instanceof Boolean) {
                    try {
                        conn.setAutoCommit((Boolean) value);
                    } catch (SQLException e) {
                        log.warn("Unable to set autocommit to value: " + value, e);
                    }
                }
            }
        };

        public abstract <T> void setValue(Connection conn, T value);
    }
    
    private final Connection conn;

    public DbConnection(Supplier<Connection> supplier) {
        if (supplier == null) {
            throw new RuntimeException("null connection supplier");
        }
        conn = supplier.get();
    }

    /**
     * Read-only. This method does not perform commits.
     * 
     * @param query
     * @param consumer
     */
    public void executeQuery(String query, Consumer<ResultSetIterable> consumer) {
        try (Statement s = conn.createStatement()) {
            consumer.accept(new ResultSetIterable(s.executeQuery(query)));
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + query, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Read-only. This method does not perform commits.
     * 
     * @param query
     * @param psConsumer
     * @param rsConsumer
     */
    public void executePreparedStatement(String query, Consumer<PreparedStatementFacade> psConsumer,
            Consumer<ResultSetIterable> rsConsumer) {
        try (PreparedStatement ps = conn.prepareStatement(query)) {
            psConsumer.accept(new PreparedStatementFacade(ps));
            rsConsumer.accept(new ResultSetIterable(ps.executeQuery()));
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + query, e);
            throw new RuntimeException(e);
        }
    }

    public int executeCountQuery(String countQuery) {
        try (PreparedStatement ps = conn.prepareStatement(countQuery)) {
            ResultSetIterable rs = new ResultSetIterable(ps.executeQuery());
            Iterator<Result> iter = rs.iterator();
            if (iter.hasNext()) {
               return iter.next().getInt(1);
            }
            rs.close();
            return 0;
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + countQuery, e);
            throw new RuntimeException(e);
        }
    }

	public int executeCountQuery(String dbStatusCount, Consumer<PreparedStatementFacade> psConsumer /*int id, String oid, String version*/) {
		// des: based on Auto-generated method stub
        try (PreparedStatement ps = conn.prepareStatement(dbStatusCount)) {
            ResultSetIterable rs = new ResultSetIterable(ps.executeQuery());
            Iterator<Result> iter = rs.iterator();
            if (iter.hasNext()) {
               return iter.next().getInt(1);
            }
            rs.close();
            return 0;
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + dbStatusCount, e);
            throw new RuntimeException(e);
        }
	}
    
    public Timestamp executeTimestampQuery(String tsQuery) {
        try (PreparedStatement ps = conn.prepareStatement(tsQuery)) {
            ResultSetIterable rs = new ResultSetIterable(ps.executeQuery());
            Iterator<Result> iter = rs.iterator();
            if (iter.hasNext()) {
               return iter.next().getTimestamp(1);
            }
            rs.close();
            return null;
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + tsQuery, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Commits.
     * 
     * @param updateSql
     */
    public void executeUpdate(String updateSql) {
        try (PreparedStatement ps = conn.prepareStatement(updateSql)) {
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + updateSql, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Commits.
     * 
     * @param updateSql
     * @param psConsumer
     */
    public void executeUpdate(String updateSql, Consumer<PreparedStatementFacade> psConsumer) {
        try (PreparedStatement ps = conn.prepareStatement(updateSql)) {
            psConsumer.accept(new PreparedStatementFacade(ps));
            ps.executeUpdate();
            conn.commit();
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + updateSql, e);
            throw new RuntimeException(e);
        }
    }

    /**
     * Does not commit.
     * 
     * @param updateSql
     * @param psConsumer
     */
    public void executeUpdateNoCommit(String updateSql, Consumer<PreparedStatementFacade> psConsumer) {
        try (PreparedStatement ps = conn.prepareStatement(updateSql)) {
            psConsumer.accept(new PreparedStatementFacade(ps));
            ps.executeUpdate();
        } catch (SQLException e) {
            log.error(e.getMessage() + "\nquery= " + updateSql, e);
            throw new RuntimeException(e);
        }
        
    }

    /**
     * Commits the connection after the consumer is finished. 
     * 
     * @param consumer
     */
    public void executeBatch(Consumer<?> consumer) {
        try {
            consumer.accept(null);
            conn.commit();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
    
    public void close() {
        try {
            conn.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public boolean isConnected() {
        if (conn == null) {
            return false;
        }
        try {
            return !conn.isClosed();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }

    public <T> void set(DbProperty prop, T value) {
        if (prop != null) {
            prop.setValue(conn, value);
        }
    }
    
}
