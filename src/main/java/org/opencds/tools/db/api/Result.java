/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.db.api;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class Result {
    private static final Log log = LogFactory.getLog(Result.class);
    
    private ResultSet rs;

    public Result(ResultSet rs) {
        this.rs = rs;
    }

    public String getString(String column) {
        try {
            return rs.getString(column);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    public String getString(int i) {
        try {
            return rs.getString(i);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    public int getInt(String column) {
        try {
            return rs.getInt(column);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    public int getInt(int column) {
        try {
            return rs.getInt(column);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }
    
    public Timestamp getTimestamp(String column) {
        try {
            return rs.getTimestamp(column);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }

    public Timestamp getTimestamp(int column) {
        try {
            return rs.getTimestamp(column);
        } catch (SQLException e) {
            log.error(e.getMessage(),e);
            throw new RuntimeException(e);
        }
    }
    
}
