/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.db.api;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;

public class PreparedStatementFacade {
    private PreparedStatement ps;

    public PreparedStatementFacade(PreparedStatement ps) {
        this.ps = ps;
    }

    public void setInt(int index, Integer value) {
        if (value == null) {
            setNull(ps, index);
        } else {
            try {
                ps.setInt(index, value);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public void setString(int index, String value) {
        if (value == null) {
            setNull(ps, index);
        } else {
            try {
                ps.setString(index, value);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
    
    public void setTimestamp(int index, Timestamp ts) {
        if (ts == null) {
            setNull(ps, index);
        } else {
            try {
                ps.setTimestamp(index, ts);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }

    public void setDouble(int index, Double value) {
        if (value == null) {
            setNull(ps, index);
        } else {
            try {
                ps.setDouble(index, value);
            } catch (SQLException e) {
                throw new RuntimeException(e.getMessage(), e);
            }
        }
    }
    
    private int getParameterType(PreparedStatement ps, int index) {
        try {
            return ps.getParameterMetaData().getParameterType(index);
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }

    private void setNull(PreparedStatement ps, int index) {
        try {
            ps.setNull(index, getParameterType(ps, index));
        } catch (SQLException e) {
            throw new RuntimeException(e.getMessage(), e);
        }
    }
    


}
