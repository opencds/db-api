/**
 * Copyright 2017-2018 OpenCDS.org
 * 
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 * 
 * http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 * 
 */
package org.opencds.tools.db.api;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.function.Consumer;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class ResultSetIterable implements Iterable<Result> {
    private static final Log log = LogFactory.getLog(ResultSetIterable.class);

    private ResultSet rs;

    public ResultSetIterable(ResultSet rs) {
        this.rs = rs;
    }

    @Override
    public Iterator<Result> iterator() {
        return new ResultSetIterator(rs);
    }

    @Override
    public void forEach(Consumer<? super Result> action) {
        Iterable.super.forEach(action);
        try {
            log.debug("Closing ResultSet...");
            rs.close();
            log.debug("ResultSet closed.");
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            // throw new RuntimeException(e);
        }
    }

    public void close() {
        try {
            rs.close();
        } catch (SQLException e) {
            log.error(e.getMessage(), e);
            // throw new RuntimeException(e);
        }
    }

}
